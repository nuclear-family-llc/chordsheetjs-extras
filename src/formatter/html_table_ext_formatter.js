import hbs from 'handlebars-inline-precompile';

import 'chordsheetjs/lib/handlebars_helpers';

const template = hbs`
{{~#with song~}}
  <div class="chord-sheet-header"><div class="chord-sheet-header-left">

      {{~#if title~}}
      <div class="chord-sheet-title">{{~title~}}</div>
      {{~/if~}}

      {{~#if subtitle~}}
      <div class="chord-sheet-subtitle">{{~subtitle~}}</div>
      {{~/if~}}

      {{~#if artist~}}
      <div class="chord-sheet-artist">{{~artist~}}</div>
      {{~/if~}}

    </div><div class="chord-sheet-header-right">

      {{~#if key~}}
      <div class="chord-sheet-key">Key of {{key~}}</div>
      {{~/if~}}

      <div class="chord-sheet-timing">{{~#if tempo~}}Tempo: {{tempo~}}{{~/if~}}{{~#if time}} | Time: {{time~}}{{~/if~}}
    </div></div></div>
  {{~#if bodyLines~}}
    <div class="chord-sheet">
      {{~#each paragraphs as |paragraph|~}}
        <div class="{{paragraphClasses paragraph}}">
          {{~#each lines as |line|~}}
            {{~#if (shouldRenderLine line)~}}
              <table class="{{lineClasses line}}">
                {{~#if (hasChordContents line)~}}
                  <tr>
                    {{~#each items as |item|~}}
                      {{~#if (isChordLyricsPair item)~}}
                        <td class="chord">{{chords}}</td>
                      {{~/if~}}
                    {{~/each~}}
                  </tr>
                {{~/if~}}
                  
                {{~#if (hasTextContents line)~}}
                  <tr>
                    {{~#each items as |item|~}}
                      {{~#if (isChordLyricsPair item)~}}
                        <td class="lyrics">{{lyrics}}</td>
                      {{~/if~}}
              
                      {{~#if (isTag item)~}}
                        {{~#if (isComment item)~}}
                          <td class="comment">{{value}}</td>
                        {{~/if~}}
                      {{~/if~}}
                    {{~/each~}}
                  </tr>
                {{~/if~}}
              </table>
            {{~/if~}}
          {{~/each~}}
        </div>
      {{~/each~}}
    </div>
  {{~/if~}}
{{~/with~}}
`;

/**
 * Formats a song into HTML. It uses TABLEs to align lyrics with chords, which makes the HTML for things like
 * PDF conversion.
 */
class HtmlTableExtFormatter {
  /**
   * Formats a song into HTML.
   * @param {Song} song The song to be formatted
   * @returns {string} The HTML string
   */
  format(song) {
    return template({ song });
  }
}

export default HtmlTableExtFormatter;